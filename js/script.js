function Summa(a, b){
    var sum=0;
    for(let i=a; i<=b;i++){
       sum+=i;
    }
    return sum;
}

function Multiplication(a, b){
    var mult=1;
     for(let i=a; i<=b;i++){
       mult*=i;
    }
    return mult;
}

function simpleNums(a, b){
    if(a==1) a=2;
    var res = [];
    for(let i = a, flag = false; i <= b; i++, flag = false){
        for(let j = 2; j * 2 <= i; j++){
            if(i % j == 0){
                flag = true;
                break;
            }
        }        
        if(!flag)
            res.push(i);
    }
    return res;
}
    

function runClick(){    
    var x1=document.getElementById('x1').value;
    var x2=document.getElementById('x2').value;
    if(x1===''||x2===''){
        alert('Поля должны быть заполнены!');
    }
    else{
        x1=parseInt(x1);
        x2=parseInt(x2);
        if(Number.isNaN(x1)||(Number.isNaN(x2))){
           alert('В поля должны быть введены числовые значения!');
        }else{
            var resultDiv=document.querySelector('#result');
            resultDiv.innerHTML='';
            if(x1<=x2){
               if(document.getElementById('summa').checked==true){
                   var sum = Summa(x1,x2);
                   resultDiv.append('Сумма всех чисел от х1 до х2 = ' +sum );
               }
                if(document.getElementById('mult').checked==true){
                   var mult = Multiplication(x1,x2);
                   resultDiv.append('Произведение всех чисел от х1 до х2 = ' +mult);
                }
                if(document.getElementById('sieve').checked==true){
                    var simpleNumbers=simpleNums(x1, x2);
                    resultDiv.append('Простые числа в промежутке от х1 до х2: ' + simpleNumbers);                    
                }
            }
             else{
                alert('x2 должно быть не менее, чем х1!');
             }
        }
    } 
}


function clearClick(){
    document.getElementById('x1').value='';
    document.getElementById('x2').value='';
}